/**
 * Renders a single article card.
 */

import PT from 'prop-types';
import React from 'react';
import { fixStyle } from 'utils/contentful';

export default function Card({ item, theme, blogFeed }) {
  return (
    <div
      className={theme.card}
      style={fixStyle(blogFeed.extraStylesForCardItem)}
    >
      <h3
        style={fixStyle(blogFeed.extraStylesForCardTitle)}
      >
        <a
          href={item.link}
          rel="noopener noreferrer"
          target="_blank"
        >
          {item.title}
        </a>
      </h3>
      <div
        style={fixStyle(blogFeed.extraStylesForCardContent)}
        /* eslint-disable react/no-danger */
        dangerouslySetInnerHTML={{ __html: item['content:encoded'] }}
        /* eslint-enable react/no-danger */
      />
      <a
        href={item.link}
        className={theme.readMore}
        target="_blank"
        rel="noopener noreferrer"
      >
Read More...
      </a>
      <div className={theme.mask} />
    </div>
  );
}

Card.propTypes = {
  item: PT.shape({
    'content:encoded': PT.string.isRequired,
    link: PT.string.isRequired,
    title: PT.string.isRequired,
  }).isRequired,
  theme: PT.shape().isRequired,
  blogFeed: PT.shape().isRequired,
};

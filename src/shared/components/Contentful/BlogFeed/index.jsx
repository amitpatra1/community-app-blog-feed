/**
 * Root BlogFeed componenent
 * It is responsible for loading the feed url and other required data from contentful
 * then pass them to the BlogFeed container
 */

import React from 'react';
import ContentfulLoader from 'containers/ContentfulLoader';
import BlogFeedContainer from 'containers/Contentful/BlogFeed';
import LoadingIndicator from 'components/LoadingIndicator';
import PT from 'prop-types';

export default function BlogFeedLoader(props) {
  const {
    id, preview, spaceName, environment,
  } = props;
  return (
    <ContentfulLoader
      entryIds={id}
      preview={preview}
      spaceName={spaceName}
      environment={environment}
      render={data => (
        <BlogFeedContainer
          {...props}
          blogFeed={data.entries.items[id].fields}
        />
      )}
      renderPlaceholder={LoadingIndicator}
    />
  );
}

BlogFeedLoader.defaultProps = {
  preview: false,
  spaceName: null,
  environment: null,
};

BlogFeedLoader.propTypes = {
  id: PT.string.isRequired,
  preview: PT.bool,
  spaceName: PT.string,
  environment: PT.string,
};

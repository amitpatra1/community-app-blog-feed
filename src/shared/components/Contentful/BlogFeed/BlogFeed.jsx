/**
 * BlogFeed rendering component
 * It will be used by the BlogFeed container to render the blog feeds
 */

import _ from 'lodash';
import LoadingIndicator from 'components/LoadingIndicator';
import PT from 'prop-types';
import React from 'react';
import { themr } from 'react-css-super-themr';
import { fixStyle } from 'utils/contentful';
import Card from './Card';
import rowLayout from './themes/rowLayout.scss';
import rowAndColumnLayout from './themes/rowAndColumnLayout.scss';
import defaultTheme from './themes/default.scss';

const THEMES = {
  RowLayout: rowLayout,
  '1N2RowsLayout': rowAndColumnLayout,
};

function BlogFeed(props) {
  const { rss, theme, blogFeed } = props;
  const { title } = blogFeed;
  let { numberOfPosts } = blogFeed;
  numberOfPosts = numberOfPosts ? parseInt(numberOfPosts, 10) : 3;
  let cards = _.get(rss, 'data.item');
  cards = cards && cards
    .filter(item => Boolean(item.category))
    .slice(0, numberOfPosts)
    .map(item => (
      <Card item={item} key={item.link} theme={theme} blogFeed={blogFeed} />
    ));

  return (
    <div
      className={theme.container}
      style={fixStyle(blogFeed.extraStylesForContainer)}
    >
      <div
        className={theme.contentWrapper}
        style={fixStyle(blogFeed.extraStylesForContentWrapper)}
      >
        {title && (
          <div
            className={theme.title}
            style={fixStyle(blogFeed.extraStylesForFeedTitle)}
          >
            {title}
          </div>
        ) }
        <div
          className={theme.cardsContainer}
          style={fixStyle(blogFeed.extraStylesForCardContainer)}
        >
          {cards || <LoadingIndicator />}
        </div>
      </div>
    </div>
  );
}

BlogFeed.defaultProps = {
  rss: null,
};

BlogFeed.propTypes = {
  rss: PT.shape({
    data: PT.shape({
      item: PT.arrayOf(PT.shape({
        link: PT.string.isRequired,
      })).isRequired,
    }),
  }),
  blogFeed: PT.shape().isRequired,
  theme: PT.shape().isRequired,
};

const ThemedBlogFeed = themr('BlogFeed', defaultTheme)(BlogFeed);

export default function BlogFeedRenderer(props) {
  const { blogFeed } = props;
  const { themeName } = blogFeed;
  const theme = THEMES[themeName];
  return (<ThemedBlogFeed {...props} theme={theme} />);
}

BlogFeedRenderer.defaultProps = {
  rss: null,
};

BlogFeedRenderer.propTypes = {
  rss: PT.shape({
    data: PT.shape({
      item: PT.arrayOf(PT.shape({
        link: PT.string.isRequired,
      })).isRequired,
    }),
  }),
  blogFeed: PT.shape().isRequired,
};

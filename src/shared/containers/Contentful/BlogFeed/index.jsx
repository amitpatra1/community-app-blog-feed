/**
 * BlogFeed container
 * It will be used by the core BlogFeed container and it will use the BlogFeed rendering
 * component to redner the blog feeds
 */

import React from 'react';
import PT from 'prop-types';
import shortId from 'shortid';
import rssActions from 'actions/rss';
import { connect } from 'react-redux';
import BlogFeed from 'components/Contentful/BlogFeed/BlogFeed';
/* Holds 1 min in ms. */
const MIN = 60 * 1000;

class BlogFeedContainer extends React.Component {
  componentDidMount() {
    this.updateRss();
  }

  updateRss() {
    const { rss, loadRss } = this.props;
    if (rss && Date.now() - rss.timestamp < 5 * MIN) return;
    loadRss();
  }


  render() {
    return (
      <BlogFeed {...this.props} />
    );
  }
}

BlogFeedContainer.defaultProps = {
  rss: null,
};

BlogFeedContainer.propTypes = {
  loadRss: PT.func.isRequired,
  rss: PT.shape({
    data: PT.object,
    loadingUuid: PT.string.isRequired,
    timestamp: PT.number.isRequired,
  }),
  blogFeed: PT.shape().isRequired,
};

function mapStateToProps(state, ownProps) {
  const rss = state.rss[ownProps.id];
  const { blogFeed } = ownProps;

  return {
    rss,
    blogFeed,
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  const a = rssActions.rss;
  const rssId = ownProps.id;
  const rssURL = `/community-app-assets/api/proxy-get?url=${encodeURIComponent(ownProps.blogFeed.rssFeedUrl)}`;
  return {
    loadRss: () => {
      const uuid = shortId();
      dispatch(a.getInit(rssId, uuid));
      dispatch(a.getDone(rssId, uuid, rssURL));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BlogFeedContainer);
